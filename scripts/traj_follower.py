#!/usr/bin/env python
import roslib
import rospy
import numpy as np
from std_msgs.msg import Float64
from std_msgs.msg import Bool
from std_msgs.msg import MultiArrayDimension
from std_msgs.msg import Float64MultiArray
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import TwistStamped
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from geometry_msgs.msg import Point
from nav_msgs.msg import Path
import math
from geometry_msgs.msg import Twist, Point
from geometry_msgs.msg import TwistStamped
from nav_msgs.msg import Path
import pure_pursuit as pp
import time
import tf

def threshold(x, min, max):
    if x>max:
        return max
    elif x<min:
        return min
    else:
        return x


class trajFollower:

    def __init__(self, name):
        self.name = name

        self.speed = 0.0
        self.angSpeed = 0.0
        self.psi = 0.0
        self.pose = PoseStamped()
        self.vel = TwistStamped()
        self.velCmd = Twist()
        self.counter = 0
        self.vel_received = False
        self.vehicle_state = 'Waiting'
        
        self.veh_name             = rospy.get_param("~veh_name","jackal")
        self.max_speed             = rospy.get_param("~max_speed",1.0)

        self.map_frame_id = rospy.get_param("~map_frame_id", "map")
        self.base_frame_id = rospy.get_param("~base_frame_id", '/'+self.veh_name+'/base_footprint')

        # for visualization
        self.marker = Marker(type=Marker.ARROW, action=Marker.ADD)
        self.L1marker = Marker(type=Marker.ARROW, action=Marker.ADD)
        self.pathmarker1 = Marker(type=Marker.LINE_STRIP, action=Marker.ADD)
        self.initMarker(self.marker)
        self.initMarker(self.L1marker)
        self.initMarker(self.pathmarker1)
        self.marker.color.a = 1.0
        self.pathmarker1.id += 1
        self.pathmarker1.scale.x = 0.035
        self.pathmarker1.color.r = 0
        self.pathmarker1.color.g = 1
        self.pathmarker1.color.a = 1

        self.L1marker.color.r = 1
        self.L1marker.color.g = 1
        self.L1marker.color.a = 1
        self.pubL1Marker = rospy.Publisher('~L1_marker', Marker,queue_size=3)
        self.pubMarker = rospy.Publisher('~marker', MarkerArray,queue_size=3)

        # L1 pure pursuit controller
        self.L1_nom             = rospy.get_param("~L1_nom",0.5)
        self.vcmd_min           = rospy.get_param("~vcmd_min", 0.2)
        self.L1_gain            = rospy.get_param("~L1_gain", 2.0)
        self.percentComplete = 1.0
        self.pp = pp.PurePursuit(np.array([[0, 0, 0, 0]]),
                                 self.L1_nom, self.L1_gain)
        
        # Mutex should prevent us from looking up purepursuit data as
        # the purepursuit module is getting updated in a threaded callback
        self.ppMutex = False
        
        # publish control commands 
        self.cmdPub = rospy.Publisher('~cmd_vel', Twist,queue_size=1)
        
        # subscribe to vehicle pose
        #rospy.Subscriber('~pose', PoseStamped, self.updatePose)
        rospy.Subscriber('~vel', Point, self.updateVel)
        rospy.Subscriber('~path', Path, self.createPathCB,queue_size=1)

        self.listener = tf.TransformListener()

        # main loop
        self.ctrl_freq = 30.0
        self.counter = 0
        self.timer_ctrl = rospy.Timer(rospy.Duration.from_sec(1.0/self.ctrl_freq), self.timerCB)

    # generate a path from a float64multiarray message
    def createPathCB(self, data):
        new_path = np.array([[x.pose.position.x, x.pose.position.y, x.pose.position.z, self.max_speed] for x in data.poses])
        # new_path = pathtoNumpyArray(data)
        new_path[-1:,-1] = 0.0 # if robot ever reaches last point on a path, probably want to set speed=0
        goal_point_2D = new_path[0,0:2]
        cur_pos = np.array([self.pose.pose.position.x, self.pose.pose.position.y])
        if (new_path.shape[0] == 1 and \
            np.linalg.norm(goal_point_2D - cur_pos) < 0.05):
            self.vehicle_state = 'Waiting'
        else: 
            self.vehicle_state = 'new_path'
            self.new_path = new_path
            # print path
            self.createBarePath(self.new_path)


    # create a path from a numpy array data set
    def createBarePath(self, data):

        self.pathmarker1.points = createPath(data[:, 0],
                                             data[:, 1],
                                             data[:, 2])
        self.pathmarker1.color.r = 0.0           
        self.pathmarker1.color.g = 1.0
        self.pathmarker1.color.b = 0.0

        # broadcast path marker for visualization purposes
        ma = MarkerArray()
        ma.markers.append(self.pathmarker1)
        self.pubMarker.publish(ma)


    def updatePose(self, msg):
        self.pose = msg
        quat = (msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w)
        euler = tf.transformations.euler_from_quaternion(quat)
        self.psi = euler[2]

    def updateVel(self, msg):
        self.vel = msg
        self.vel_received = True
    
    def computeControl(self):
        if not self.ppMutex:
            # get current position and velocity in numpy formats
            p = np.array([self.pose.pose.position.x,
                          self.pose.pose.position.y,
                          self.pose.pose.position.z])
            if self.vel_received:
                v = np.array([self.vel.x,
                              self.vel.y,
                              self.vel.z])
            else:
                v = np.array([self.speed * math.cos(self.psi),
                              self.speed * math.sin(self.psi),
                              0])

            # calculate pure pursuit control vectors
            pL1, v_cmd, r_cmd, psi_cmd, index_L1, index_close = self.pp.calcL1Control(p, v, self.psi)
            # rospy.loginfo('L1: %s' %pL1)
            self.L1marker.pose.position.x = pL1[0]
            self.L1marker.pose.position.y = pL1[1]
            self.L1marker.pose.orientation.w = math.cos(psi_cmd / 2.0)
            self.L1marker.pose.orientation.z = math.sin(psi_cmd / 2.0)
            self.pubL1Marker.publish(self.L1marker)

            r_cmd = r_cmd * -1
            
            path_length = self.pp.path.shape[0]
            if path_length < 2:
                self.percentComplete = 1.0
            else:
                self.percentComplete = max(float(index_L1) / (path_length-1), self.percentComplete)

            ### Highly experimental... not sure if a good idea
            # If the MP isn't that long, there's no reason to drive at true max speed
            if path_length < 30:
                v_cmd = saturate(v_cmd, 0.5, -0.5)

            
            # saturate commanded linear speed
            v_cmd = saturate(v_cmd, self.max_speed, -self.max_speed)

            # compute closest point projection
            pL1 = self.pp.path[index_L1, 0:2]

            # generate marker data for visualization
            self.marker.pose.position.x = pL1[0]
            self.marker.pose.position.y = pL1[1]
            self.marker.pose.orientation.w = math.cos(psi_cmd / 2.0)
            self.marker.pose.orientation.z = math.sin(psi_cmd / 2.0)

            # at end of path
            L1_pt =  pL1
            goal =  self.pp.path[path_length-1, 0:2]
            dist_to_goal =  math.sqrt(math.pow(self.pose.pose.position.x - goal[0],2)\
                + math.pow(self.pose.pose.position.y - goal[1],2))
            angle_to_L1_pt =  math.atan2(L1_pt[1]-self.pose.pose.position.y, L1_pt[0] - self.pose.pose.position.x)

            # keep heading error between [-pi, pi]
            heading_error = wrap(angle_to_L1_pt - self.psi)

            kp_v = 0.5      # gain of how much to slow down if close to goal
            kp_r = 1        # gain of how much turning rate should decrease near goal   

            if dist_to_goal < 2.0: # slow down linearly when close to goal
                # print("getting close to goal: {}".format(dist_to_goal))
                v_cmd = saturate(kp_v * (dist_to_goal - 0.1), v_cmd, 0.0)
                r_cmd = r_cmd * saturate(kp_r * (dist_to_goal - 0.1), 1.0, 0.0)
            if abs(heading_error) > np.pi/4: # need to spin in place before moving forward
                # print("heading error is too big. spinning in place.")
                v_cmd = 0
                r_cmd = 1.5 * kp_r * heading_error
            if (dist_to_goal < 0.1 and self.percentComplete>=0.9 and self.vehicle_state!='new_path')\
                or self.vehicle_state == 'Waiting':
                self.vehicle_state = 'Waiting'
                # print("stopped. waiting.")
                v_cmd = 0
                r_cmd = 0
            # else:
            #     print("good to go!")
            
            # Update linear, angular speeds that are sent to vehicle
            self.speed = v_cmd
            self.angSpeed = r_cmd

            if self.vehicle_state == 'new_path':
                self.vehicle_state = "Driving"
                self.percentComplete = 0
                self.pp.updateTree(self.new_path)

    def publishPath(self):
        if self.percentComplete == 1.0:
            return
        ma = MarkerArray()
        ma.markers.append(self.marker)
        if self.percentComplete < 0.9:
            ma.markers.append(self.pathmarker1)
        #  self.pubMarker.publish(ma)

    # setup goal marker for visualization in rviz
    def initMarker(self, m):
        m.header.frame_id = self.map_frame_id
        m.header.stamp = rospy.Time.now()
        m.pose.position.x = 0.0
        m.pose.position.y = 0.0
        m.pose.position.z = 0.0

        m.ns = "RAVEN_rover"
        ns = rospy.get_namespace()
        m.id = 0
        for letter in str(ns):
            m.id += ord(letter)  # cheap way to get a unique marker id

        m.pose.orientation.x = 0
        m.pose.orientation.y = 0
        m.pose.orientation.z = 0.0
        m.pose.orientation.w = 1
        m.scale.x = 0.5
        m.scale.y = 0.03
        m.scale.z = 0.03
        m.color.r = 1.0
        m.color.g = 0.0
        m.color.b = 0.0
        m.color.a = 1.0
        m.lifetime = rospy.Duration(10)
        return m
    




    # ##############################################################
    # publish control command
    #
    # ##############################################################
    def publishCmd(self):
        self.velCmd.linear.x = self.speed
        self.velCmd.angular.z = self.angSpeed
        self.cmdPub.publish(self.velCmd)
        
    def timerCB(self, event):
        try:
            (trans,rot) = self.listener.lookupTransform(self.map_frame_id, self.base_frame_id, rospy.Time(0))
            self.pose.pose.position.x = trans[0]
            self.pose.pose.position.y = trans[1]
            self.pose.pose.position.z = trans[2]
            self.pose.pose.orientation.x = rot[0]
            self.pose.pose.orientation.y = rot[0]
            self.pose.pose.orientation.z = rot[0]
            self.pose.pose.orientation.w = rot[0]
            euler = tf.transformations.euler_from_quaternion(rot)
            self.psi = euler[2]
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            print("error finding veh pos...")

        self.computeControl()
        self.publishCmd()
        self.counter += 1
        if self.counter == 50:
            self.publishPath()
            self.counter = 0

# form the discretized position variables into a ros navigation path message
def createPath(x, y, z):
    points = []
    for i in range(len(x)):
        p = Point()
        p.x = x[i]
        p.y = y[i]
        p.z = z[i]
        points.append(p)
    return points

#-----------------------------------------------------------------------------
# 2D Multiarray to numpy array -- convert multiarray data type to a numpy array
# I'm sure there are more efficient ways to do this
#-----------------------------------------------------------------------------
def multiarray2DToNumpyArray(ma):
    I = ma.layout.dim[0].size
    J = ma.layout.dim[1].size
    # print I,J
    na = np.empty([I, J])
    for i in range(I):
        for j in range(J):
            index = ma.layout.data_offset + ma.layout.dim[1].stride * i + j
            na[i, j] = ma.data[index]
    return na

#-----------------------------------------------------------------------------
# 2D numpy array to multiarray
# I'm sure there are more efficient ways to do this
#-----------------------------------------------------------------------------
def numpyArray2DToMultiarray(na, ma):
    I = na.shape[0]
    J = na.shape[1]
    d1 = MultiArrayDimension()
    d2 = MultiArrayDimension()
    ma.layout.dim = [d1, d2]
    ma.layout.dim[0].size = I
    ma.layout.dim[0].stride = I * J
    ma.layout.dim[1].size = J
    ma.layout.dim[1].stride = J
    for i in range(I):
        ma.data = np.append(ma.data, na[i, :])

def saturate(val, high, low, sat=[0]):
    sat[0] = 0
    if val > high:
        val = high
        sat[0] = 1
    if val < low:
        val = low
        sat[0] = -1
    return val

def wrap(angle):
    if angle > math.pi:
        angle -= 2*math.pi
    elif angle < -math.pi:
        angle += 2*math.pi
    return angle

if __name__ == '__main__':
    try:
        ns = rospy.get_namespace()
        rospy.init_node('roverTrajFollower')
        if str(ns) == '/':
            rospy.logfatal("Need to specify namespace as vehicle name.")
            rospy.logfatal("This is typically accomplished in a launch file.")
            rospy.logfatal("Command line: ROS_NAMESPACE=uP04 $ rosrun upucc_control path_follower.py")
        else:
            rospy.loginfo("Starting project on path for: " + str(ns))
            traj_follower = trajFollower(str(ns))
            rospy.spin()
    except rospy.ROSInterruptException:
        pass
