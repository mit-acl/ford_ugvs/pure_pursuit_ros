#!/usr/bin/env python

import rospy

from geometry_msgs.msg import Vector3
from geometry_msgs.msg import PoseStamped, PoseWithCovarianceStamped
from nav_msgs.msg import Odometry
import numpy as np
import tf
import pickle
import time

class PoseToVelNode(object):
    def __init__(self):
        self.node_name = rospy.get_name()
        self.filename = rospy.get_param("~filename","/home/swarm/last_pose.p")

        self.pub_vel = rospy.Publisher("~vel", Vector3, queue_size=1)
        self.pub_pose_est = rospy.Publisher("~pose_est", PoseWithCovarianceStamped, queue_size=1)
        self.first = True
        self.vx_list = []
        self.num_poses = 5
        self.last_pose = PoseStamped()
        self.vel_list = []
        self.pose = None

        self.sub_pose = rospy.Subscriber("~pose", PoseStamped, self.cbPose)
        self.sub_odom = rospy.Subscriber("~odom", Odometry, self.cbOdom)

        # Lookup last recorded pose and publish it as initialpose
        time.sleep(5.0)
        last_pose = None
        try:
            f = open(self.filename,'r')
            last_pose = pickle.load(f)
            f.close()
        except:
            rospy.logwarn("Couldn't open pickle file of last pose: %s" %self.filename)

        if isinstance(last_pose,PoseStamped):
            rospy.loginfo("[%s] Loading last pose..."%(self.node_name))
            pose_est = PoseWithCovarianceStamped()
            pose_est.pose.covariance = [0.25, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.25, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.06853891945200942]
            pose_est.pose.pose = last_pose.pose
            pose_est.header.frame_id = "map"
            pose_est.header.stamp = rospy.Time.now()
            self.pub_pose_est.publish(pose_est)    


    def cbPose(self,pose_msg):
        self.pose = pose_msg

    def cbOdom(self,odom_msg):
        if self.pose is not None:
            if len(self.vx_list) < self.num_poses:            
                self.vx_list.append(odom_msg.twist.twist.linear.x)
                return
            else:
                self.vx_list.pop(0)
                self.vx_list.append(odom_msg.twist.twist.linear.x)
                
                v_mag = np.median(self.vx_list)
                quaternion = (
                    self.pose.pose.orientation.x,
                    self.pose.pose.orientation.y,
                    self.pose.pose.orientation.z,
                    self.pose.pose.orientation.w)
                euler = tf.transformations.euler_from_quaternion(quaternion)
                yaw = euler[2]
                v = Vector3()
                v.x = v_mag * np.cos(yaw)
                v.y = v_mag * np.sin(yaw)
                self.pub_vel.publish(v)

    def rotate(self, l, x):
        return l[-x:] + l[:-x]

    def on_shutdown(self):
        f = open(self.filename,'wb')
        pickle.dump(self.pose,f)
        f.close()



if __name__ == '__main__':
    rospy.init_node('pose_to_vel_node',anonymous=False)
    pose_to_vel_node = PoseToVelNode()
    rospy.on_shutdown(pose_to_vel_node.on_shutdown)
    rospy.spin()