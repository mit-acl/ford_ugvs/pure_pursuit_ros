#!/usr/bin/env python
import roslib
import rospy
import numpy as np
from std_msgs.msg import Float64, Bool, MultiArrayDimension, Float64MultiArray, ColorRGBA
from geometry_msgs.msg import PoseStamped, Vector3, Point
from visualization_msgs.msg import Marker, MarkerArray
from nav_msgs.msg import Path
import math
import pure_pursuit as pp
import time
import tf
import dynamic_reconfigure.client

class PathToGoalPos:

    def __init__(self):

        self.param_client = dynamic_reconfigure.client.Client("planner_fsm", timeout=2, config_callback=self.cbDynParam)

        self.path = None

        # L1 pure pursuit controller
        self.L1_nom             = rospy.get_param("~L1_nom",2.0)
        self.L1_gain            = rospy.get_param("~L1_gain", 2.0)
        self.pp = pp.PurePursuit(np.array([[0, 0, 0, 0]]),
                                 self.L1_nom, self.L1_gain)
        
        # publish goal position 
        self.pub_goal = rospy.Publisher('~goal', PoseStamped, queue_size=1)

        # subscribe to vehicle pose
        rospy.Subscriber('~pose', PoseStamped, self.cbPose)
        rospy.Subscriber('~path', Float64MultiArray, self.cbPath, queue_size=1)

        # main loop
        self.ctrl_freq = 10.0
        self.timer_ctrl = rospy.Timer(rospy.Duration.from_sec(1.0/self.ctrl_freq), self.cbTimer)
    
    def cbDynParam(self, config):
        self.max_speed = config.robot_max_speed
    
    def cbPose(self, msg):
        self.pose = msg
        quat = (msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w)
        euler = tf.transformations.euler_from_quaternion(quat)
        self.psi = euler[2]

    # generate a path from a float64multiarray message
    def cbPath(self, data):
        path = multiarray2DToNumpyArray(data)
        path[-1:,-1] = 0.0 # if robot ever reaches last point on a path, probably want to set speed=0
        self.pp.updateTree(path)
        self.path = path

    def cbTimer(self, event):
        if self.path is None:
            return

        # calculate L1 pt from pure pursuit kd tree representation of path
        p = np.array([self.pose.pose.position.x,
                      self.pose.pose.position.y,
                      self.pose.pose.position.z])
        index_close, index_L1 = self.pp.findL1(p)
        pL1 = self.pp.path[index_L1, 0:-2]

        goal_msg = PoseStamped()
        goal_msg.header.stamp = rospy.Time.now()
        goal_msg.header.frame_id = 'map_1'
        goal_msg.pose.position.x = pL1[0]
        goal_msg.pose.position.y = pL1[1]
        self.pub_goal.publish(goal_msg)
    

#-----------------------------------------------------------------------------
# 2D Multiarray to numpy array -- convert multiarray data type to a numpy array
# I'm sure there are more efficient ways to do this
#-----------------------------------------------------------------------------
def multiarray2DToNumpyArray(ma):
    I = ma.layout.dim[0].size
    J = ma.layout.dim[1].size
    # print I,J
    na = np.empty([I, J])
    for i in xrange(I):
        for j in xrange(J):
            index = ma.layout.data_offset + ma.layout.dim[1].stride * i + j
            na[i, j] = ma.data[index]
    return na

if __name__ == "__main__":
    rospy.init_node("traj_to_goal_pos", anonymous=True)
    t = PathToGoalPos()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("shutting down")